import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Inspire from "../views/Inspire.vue";
import About from "../views/About.vue";

Vue.use(VueRouter);

const router = new VueRouter({
    base: "/",
    mode: "history",
    routes: [
        { path: "/", component: Home },
        { path: "/inspire", component: Inspire },
        { path: "/about", component: About },
    ],
});

export default router;